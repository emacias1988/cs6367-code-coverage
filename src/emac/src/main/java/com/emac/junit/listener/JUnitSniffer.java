package com.emac.junit.listener;

import org.junit.runner.Description;
import org.junit.runner.Result;
import org.junit.runner.notification.Failure;
import org.junit.runner.notification.RunListener;

import com.emac.parser.CoverageParser;

public class JUnitSniffer extends RunListener
{
	public void testRunStarted(Description description) throws Exception {
		CoverageParser.testSuiteStart();
    }

	public void testRunFinished(Result result) throws Exception {
		CoverageParser.testSuiteFinish();
	}

	public void testStarted(Description description) throws Exception {
		CoverageParser.testCaseStart(description.getClassName()+"."+description.getMethodName());
	}

	public void testFinished(Description description) throws Exception {
		CoverageParser.testCaseFinish();
	}

	public void testFailure(Failure failure) throws Exception {
		CoverageParser.testCaseFinish();
	}

}
