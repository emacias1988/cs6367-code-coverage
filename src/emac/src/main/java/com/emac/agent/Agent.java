package com.emac.agent;

import org.objectweb.asm.ClassReader;
import org.objectweb.asm.ClassWriter;

import java.lang.instrument.ClassFileTransformer;
import java.lang.instrument.IllegalClassFormatException;
import java.lang.instrument.Instrumentation;
import java.security.ProtectionDomain;

public class Agent {
	
    public static void premain(String agentArgs, Instrumentation inst) {
        inst.addTransformer(new ClassFileTransformer() {
        	
            @Override
            public byte[] transform(ClassLoader classLoader, String classname, Class<?> classRedefined, ProtectionDomain protectionDomain, byte[] classFileBuffer) throws IllegalClassFormatException {
            	byte[] byteCode = classFileBuffer;
            	if(classname.replace("/", ".").startsWith(System.getProperty("testProjectPackageName")))
            	{
					ClassReader cr = new ClassReader(classFileBuffer);
					ClassWriter cw = new ClassWriter(ClassWriter.COMPUTE_FRAMES);
					ClassTransformVisitor ca = new ClassTransformVisitor(cw);
					cr.accept(ca, 0);
					return cw.toByteArray();
                }
            	else
            	{
            		 return byteCode;
            	}
            }
        });
    }

}

