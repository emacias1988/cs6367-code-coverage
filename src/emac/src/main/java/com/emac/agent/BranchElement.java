package com.emac.agent;

public class BranchElement {
	private int lineNumber;
	private int opCode;
	private String branchLineLabel;
	
	public BranchElement(int lineNum, int opCode, String lineLab)
	{
		this.lineNumber = lineNum;
		this.opCode = opCode;
		this.branchLineLabel = lineLab;
	}
	
	public String getBranchLine()
	{
		return this.branchLineLabel;
	}
	
	public int getLineNumber()
	{
		return this.lineNumber;
	}
	
	public int getOpCode()
	{
		return this.opCode;
	}
	
	public void setBranchLineLabel(String lineLab)
	{
		this.branchLineLabel = lineLab;
	}
	
	public String toString()
	{
		return "Line: "+lineNumber+" OpCode: "+opCode+" Jump Label: "+branchLineLabel;
	}
	
}
