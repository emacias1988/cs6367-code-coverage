package com.emac.agent;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Date;

public class FileLogger {
	
	static File logFile;
	static String fPath;
	static FileWriter fw;
	
	public final static void open(String fileName, String filePath)
	{
		fPath = "target/CFG/"+filePath+"/";
		createDirectory();
		logFile = new File(fPath+fileName+".dot");
		 try {
            fw = new FileWriter(logFile.getAbsoluteFile(),false);
        } catch (IOException ex) {
            System.err.println("Error creating File: "+logFile.getAbsoluteFile()+". Ex:"+ex);
        }
	}
	
	public final static void log(String s)
	{
        try {
            fw.write(s);
            fw.write(System.lineSeparator());
        } catch (IOException ex) {
            System.err.println("Error logging:"+s+". Ex:"+ex);
        }	
	}
	
	public final static void close()
	{
		 try {
			 fw.close();
        } catch (IOException ex) {
            System.err.println("Error clsoing FW. Ex:"+ex);
        }
	}
	
	public final static void createDirectory()
	{
		File logDir = new File(fPath);
		if (!logDir.exists()) 
		{
			System.out.println("<CFG> Creating DIR "+fPath);
		    try{
		        logDir.mkdirs();
		    } catch(SecurityException se){
		        //handle it
		    	System.err.println("Error creating dir FW. Ex:"+se);
		    }
		}
	}
}
