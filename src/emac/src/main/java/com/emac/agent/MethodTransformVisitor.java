package com.emac.agent;

import java.util.*;
import org.objectweb.asm.Label;
import org.objectweb.asm.MethodVisitor;
import org.objectweb.asm.Opcodes;

import com.emac.parser.CoverageParser;

class MethodTransformVisitor extends MethodVisitor implements Opcodes {

	// Method Name
	private String mName;
	// Class Name
	private String cName;
	// Make a set for branches
	private Set<Integer> branchLineNumSet;
	// Make a set
	private List<Integer> stmtsList;
	// Current Line used to add line to branch line
	private int curLine=-1;
	
	// Set of LineElements (stmts visited)
	private List<LineElement> lineEleList;
	// Make a set for branches
	private List<BranchElement> branchEleSet;
	
	private Set<Integer> returnOpCodes;
	
	// areturn-0xb0, dreturn-0xaf, freturn-0xae, ireturn-0xac, lreturn-0xad, return-0xb1, athrow-0xbf
	private final Integer[] retOpArray = new Integer[]{0xb0,0xaf,0xae,0xac,0xad,0xb1,0xbf};
	
	private Hashtable<String,Integer> stmtLabels;
	
	// Make a set for branches
	private Set<Integer> returnLinesSet;
	
	private Hashtable<Integer,LineElement> finalElementhash;
	
	private boolean generateCFG;
	
    public MethodTransformVisitor(final MethodVisitor mv, String name, String cName) {
        super(ASM5, mv);
        this.mName=name;
		this.cName=cName;
		this.stmtsList = new ArrayList<Integer>();
		this.branchLineNumSet = new HashSet<Integer>();
		this.lineEleList = new ArrayList<LineElement>();
		this.branchEleSet = new ArrayList<BranchElement>();
		this.returnOpCodes = new HashSet<Integer>(Arrays.asList(retOpArray));
		this.stmtLabels = new Hashtable<String,Integer>();
		this.returnLinesSet =  new HashSet<Integer>();
		this.finalElementhash = new Hashtable<Integer,LineElement>();
		
		// Check if this class should generated a class flow graph
		if(System.getProperty("cfgClassNames") != null && System.getProperty("cfgClassNames").contains(cName.replace("/", ".")))
		{
			this.generateCFG = true;
		}
		else
		{
			this.generateCFG = false;
		}
    }
	
	@Override
	public void visitInsn(int opcode)
	{
		// RETURN OP CODES
		if(this.returnOpCodes.contains(opcode))
		{
			this.returnLinesSet.add(curLine);
		}
		
		super.visitInsn(opcode);
	}
	
	@Override
	public void visitLabel(Label label)
	{
    	if(curLine != -1)
    	{
    		mv.visitLdcInsn(cName);
    		mv.visitLdcInsn(String.valueOf(curLine));
    		mv.visitMethodInsn(INVOKESTATIC, "com/emac/parser/CoverageParser", "logLine", "(Ljava/lang/String;Ljava/lang/String;)V", false);
    	}

		if(curLine > 0)
		{
			if(generateCFG)
			{
				//Add label to current line
				stmtLabels.put(label.toString(), curLine);
				// Create a new Element
				LineElement lineEle = new LineElement(curLine,label.toString());
				// Add the element to the list
				this.lineEleList.add(lineEle);
			}
		}
		super.visitLabel(label);
	}
	
	@Override
	public void visitJumpInsn(int opcode, Label label)
	{
		if(generateCFG)
		{
			BranchElement branchEle = new BranchElement(curLine,opcode,label.toString());
			this.branchEleSet.add(branchEle);
		}
		// Check that is not a GOTO statement
		if(opcode != 167)
		{
			this.branchLineNumSet.add(curLine);
		}
		mv.visitLdcInsn(cName);
		mv.visitLdcInsn(String.valueOf(curLine));
		mv.visitMethodInsn(INVOKESTATIC, "com/emac/parser/CoverageParser", "logLine", "(Ljava/lang/String;Ljava/lang/String;)V", false);
		super.visitJumpInsn(opcode,label);
	}

    @Override
    public void visitCode(){
    	
		// Store method name in file
		mv.visitLdcInsn(cName);
		mv.visitLdcInsn(mName);
		mv.visitMethodInsn(INVOKESTATIC, "com/emac/parser/CoverageParser", "logMethod", "(Ljava/lang/String;Ljava/lang/String;)V", false);
    	super.visitCode();
    }

	@Override
	public void visitLineNumber(int line, Label start){

		
		if(generateCFG)
		{
			//Add label to current line
			stmtLabels.put(start.toString(), line);
			// Create a new Element
			LineElement lineEle = new LineElement(line,start.toString());
			// Add the element to the list
			this.lineEleList.add(lineEle);
		}
    	
		// Copy current line
		this.curLine = line;
		// Include statement List
		this.stmtsList.add(line);

		// Store line number executed in log file per each class file
		mv.visitLdcInsn(cName);
		mv.visitLdcInsn(String.valueOf(line));
		mv.visitMethodInsn(INVOKESTATIC, "com/emac/parser/CoverageParser", "logLine", "(Ljava/lang/String;Ljava/lang/String;)V", false);
    	super.visitLineNumber(line,start);
	}
	
	@Override
	public void visitEnd(){
		
		if(generateCFG)
		{
			LineElement tempLineEle;
			
			// Add branch statements
			for(int i=0; i< branchEleSet.size(); i++)
			{
				if(finalElementhash.containsKey(branchEleSet.get(i).getLineNumber()))
				{
					tempLineEle = finalElementhash.get(branchEleSet.get(i).getLineNumber());
				}
				else 
				{
					tempLineEle = new LineElement(branchEleSet.get(i).getLineNumber());
					
					this.finalElementhash.put(branchEleSet.get(i).getLineNumber(),tempLineEle);
				}
				
				if(branchEleSet.get(i).getOpCode() == 167)
				{
					tempLineEle.addNextElement(stmtLabels.get(branchEleSet.get(i).getBranchLine()));
				}
				// Make sure that it is not jumping to itself
				else if(stmtLabels.get(branchEleSet.get(i).getBranchLine()) != branchEleSet.get(i).getLineNumber())
				{
					tempLineEle.setTrueBranch(stmtLabels.get(branchEleSet.get(i).getBranchLine()));
					int lastIdx=0;
					for(int x=0;x<lineEleList.size();x++)
					{
						if(x != (lineEleList.size()-1) && branchEleSet.get(i).getLineNumber() == lineEleList.get(x).getLineNumber())
						{
							lastIdx = x;
						}
					}
					tempLineEle.setFalseBranch(lineEleList.get(lastIdx+1).getLineNumber());
				}
			}
			
//			System.out.println("<CFG> Branch Elements Created "+ cName.replace("/", "."));
			
			for(int i=0; i< lineEleList.size(); i++)
			{
				// Check if the line is a return statement
				if(this.returnLinesSet.contains(lineEleList.get(i).getLineNumber()))
				{
					if(finalElementhash.containsKey(lineEleList.get(i).getLineNumber()))
					{
						tempLineEle = finalElementhash.get(lineEleList.get(i).getLineNumber());
					}
					else 
					{
						tempLineEle = new LineElement(lineEleList.get(i).getLineNumber());
						
						this.finalElementhash.put(lineEleList.get(i).getLineNumber(),tempLineEle);
					}
				}
				else if(i != (lineEleList.size()-1) && lineEleList.get(i).getLineNumber() != lineEleList.get(i+1).getLineNumber())
				{
					if(finalElementhash.containsKey(lineEleList.get(i).getLineNumber()))
					{
						tempLineEle = finalElementhash.get(lineEleList.get(i).getLineNumber());
					}
					else 
					{
						tempLineEle = new LineElement(lineEleList.get(i).getLineNumber());
						
						this.finalElementhash.put(lineEleList.get(i).getLineNumber(),tempLineEle);
					}
					tempLineEle.addNextElement(lineEleList.get(i+1).getLineNumber());
				}
				else if(i != (lineEleList.size()-1) && lineEleList.get(i).getLineNumber() == lineEleList.get(i+1).getLineNumber())
				{
					if(finalElementhash.containsKey(lineEleList.get(i).getLineNumber()) && 
							finalElementhash.get(lineEleList.get(i).getLineNumber()).getFalseBranch() == -1)
					{
						// Skip the next element
						i++;
					}
				}
			}
			createCFG();
		}
        
		if(!stmtsList.isEmpty())
		{
			CoverageParser.logClassInfo(cName,mName, stmtsList, branchLineNumSet);
		}
        super.visitEnd();
	}
	
	private void createCFG()
	{
		String fileName = cName.substring(cName.lastIndexOf('/')+1)+"."+mName;
		fileName = fileName.replace("/", ".");
		FileLogger.open(fileName, cName);
		FileLogger.log("digraph \""+fileName+"\" {");
		FileLogger.log(" graph [label=\""+fileName+"\", rankdir=\"LR\"];");
		
		Hashtable <Integer,Integer> cfgLabels = new Hashtable<Integer,Integer>();
		int labelInt=0;
		
		List<Integer> tepList = Collections.list(finalElementhash.keys());
		Collections.sort(tepList);
		Iterator<Integer> it = tepList.iterator();
		
		FileLogger.log(" "+labelInt+" [label=\"START\", shape=box, style=\"filled, rounded\", fillcolor=\"#E6E6E6\", fixedsize=true, fontsize=12, width=0.6, height=0.6 ]");
		labelInt++;
		FileLogger.log(" "+labelInt+" [label=\"END\", shape=box, style=\"filled, rounded\", fillcolor=\"#E6E6E6\", fixedsize=true, fontsize=12, width=0.6, height=0.6 ]");
		labelInt++;
		
		// Check if there are dangerous edges
		String classesProp = System.getProperty("cfgClassNames");
		String[] classPropArray = classesProp.split(",");
		Set<Integer> dangerEdgeSet = new HashSet<Integer>();
		
		// Copy the line numbers to dangerEdge
		for(String classProp : classPropArray)
		{
			if(classProp.contains(cName.replace("/", ".")))
			{
				if(classProp.contains("[") && classProp.contains("]") && classProp.length() > 2)
				{
					classProp = classProp.substring(classProp.indexOf("[")+1, classProp.length()-1);
					String[] dangEdgesString = classProp.split("/");
					// Copy Edges 
					for(int i=0; i<dangEdgesString.length; i++)
					{
						dangerEdgeSet.add(Integer.parseInt(dangEdgesString[i])); 
					}
				}
				break;
			}
		}
		
		// Create Elements
		while(it.hasNext()){
			Integer element =it.next();
			LineElement lineEle = finalElementhash.get(element);
			// Store the current label to the current line number
			cfgLabels.put(lineEle.getLineNumber(),labelInt);
			if(lineEle.getTrueBranch() == -1 && lineEle.getFalseBranch() == -1 && lineEle.getSuccessor().isEmpty())
			{
				FileLogger.log(" "+labelInt+" [label=\""+lineEle.getLineNumber()+"\", shape=invhouse, style=filled, fillcolor=\"#FFDCA8\", fixedsize=true, fontsize=12, width=0.78, height=0.36 ]");
			}
			else if(lineEle.getTrueBranch() != -1 && lineEle.getFalseBranch() != -1 && lineEle.getSuccessor().isEmpty())
			{
				FileLogger.log(" "+labelInt+" [label=\""+lineEle.getLineNumber()+"\", shape=diamond, style=filled, fillcolor=\"#B4FFB4\", fixedsize=true, fontsize=12, width=0.78, height=0.36 ]");
			}
			else
			{
				FileLogger.log(" "+labelInt+" [label=\""+lineEle.getLineNumber()+"\", shape=box, style=filled, fillcolor=\"#CECEFF\", fixedsize=true, fontsize=12, width=0.78, height=0.36 ]");
			}
			
			labelInt++;
		}
		
		// Edge Color
		String edgeColor;
		// Edge label 
		String edgeLabel;
		
		// Check if first line has a dangerous edge
		if(dangerEdgeSet.contains(stmtsList.get(0)))
		{
			edgeColor="color=red";
			edgeLabel = "(D.E.)\", fontcolor=red";
		}
		else
		{
			edgeColor="color=black";
			edgeLabel = "\"";
		}
		
		// Add a dashed line from start to first element
		FileLogger.log(" 0 -> 2 [label=\""+edgeLabel+", style=dashed, "+edgeColor+" ]");
		
		// Reset the Iterator
		it = tepList.iterator();
		
		while(it.hasNext()){
			Integer element =it.next();
			LineElement lineEle = finalElementhash.get(element);
			
			// Check for Last Edges
			if(lineEle.getTrueBranch() == -1 && lineEle.getFalseBranch() == -1 && lineEle.getSuccessor().isEmpty())
			{
				FileLogger.log(" "+cfgLabels.get(lineEle.getLineNumber())+" -> 1 [label=\"\", style=dashed ]");
			}
			else if(lineEle.getTrueBranch() != -1 && lineEle.getFalseBranch() != -1)
			{
				// Check if True Branch has a dangerous edge
				if(dangerEdgeSet.contains(lineEle.getTrueBranch()))
				{
					edgeColor="color=red";
					edgeLabel = "(D.E.)\", fontcolor=red";
				}
				else
				{
					edgeColor="color=black";
					edgeLabel = "\"";
				}
				// Get True Branch
				FileLogger.log(" "+cfgLabels.get(lineEle.getLineNumber())+" -> "+cfgLabels.get(lineEle.getTrueBranch())+" [label=\"True "+edgeLabel+", style=solid, "+edgeColor+" ]");
				// Check if False Branch has a dangerous edge
				if(dangerEdgeSet.contains(lineEle.getFalseBranch()))
				{
					edgeColor="color=red";
					edgeLabel = "(D.E.)\", fontcolor=red";
				}
				else
				{
					edgeColor="color=black";
					edgeLabel = "\"";
				}
				// Get False Branch
				FileLogger.log(" "+cfgLabels.get(lineEle.getLineNumber())+" -> "+cfgLabels.get(lineEle.getFalseBranch())+" [label=\"False "+edgeLabel+", style=solid, "+edgeColor+" ]");
			}
			
			if(!lineEle.getSuccessor().isEmpty())
			{
				// Get successors
				for(Integer successor: lineEle.getSuccessor())
				{
					// Check if successor has a dangerous edge
					if(dangerEdgeSet.contains(successor))
					{
						edgeColor="color=red";
						edgeLabel = "(D.E.)\", fontcolor=red";
					}
					else
					{
						edgeColor="color=black";
						edgeLabel = "\"";
					}
					FileLogger.log(" "+cfgLabels.get(lineEle.getLineNumber())+" -> "+cfgLabels.get(successor)+" [label=\""+edgeLabel+", style=solid, "+edgeColor+" ]");
				}
			}
		}
		
		FileLogger.log("}");
		FileLogger.close();
		System.out.println("<CFG> Generated CFG "+fileName+".dot Path: /target/CFG/"+cName);
	}

}