package com.emac.agent;

import java.util.HashSet;
import java.util.Set;

public class LineElement {
	
	private int lineNumber;
	private String lineLabel;
	// Make a set for branches
	private Set<Integer> successor;
	private int branchTrue;
	private int branchFalse;
	
	public LineElement(int lineNum, String lineLab)
	{
		this.lineNumber = lineNum;
		this.lineLabel = lineLab;
		successor = new HashSet<Integer>();
		branchTrue = -1;
		branchFalse = -1;
	}
	
	public LineElement(int lineNum)
	{
		this.lineNumber = lineNum;
		this.lineLabel = "";
		successor = new HashSet<Integer>();
		branchTrue = -1;
		branchFalse = -1;
	}
	
	public String getLineLabel()
	{
		return this.lineLabel;
	}
	
	public int getLineNumber()
	{
		return this.lineNumber;
	}
	
	public Set<Integer> getSuccessor()
	{
		return this.successor;
	}
	
	public int getTrueBranch()
	{
		return this.branchTrue;
	}
	
	public int getFalseBranch()
	{
		return this.branchFalse;
	}
	
	public void addNextElement(int nextEle)
	{
		if(nextEle != this.lineNumber 
				&& nextEle != this.branchTrue
				&& nextEle != this.branchFalse)
		{
			this.successor.add(nextEle);
		}
	}
	
	public void setTrueBranch(int trueBranchLine)
	{
		this.branchTrue = trueBranchLine;
	}
	
	public void setFalseBranch(int falseBrancLine)
	{
		this.branchFalse = falseBrancLine;
	}
	
	public String toString()
	{
		return "Line: "+lineNumber+" Label: "+lineLabel+" NS: "+this.getSuccessor()+" BS: T="+this.getTrueBranch()+" F:"+this.getFalseBranch();
	}
	
	

}
