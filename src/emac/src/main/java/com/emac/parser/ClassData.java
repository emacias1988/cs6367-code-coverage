package com.emac.parser;

import java.util.*;

public class ClassData
{
	String className;
	
	// Make a set for branches
	private Set<Integer> branchLineNumSet;
	// List of all statments
	private List<Integer> stmtsList;
	// Set of branch paths
	private Set<String> branchPathsSet;
	// Set of method names
	private Set<String> methodNamesSet;
	// Modified Statements
	private Set<Integer> modStmts;
	
	public ClassData(String cName)
	{
		this.className = cName;
		this.stmtsList = new ArrayList<>();
		this.branchLineNumSet = new HashSet<>();
		this.branchPathsSet = new HashSet<>();
		this.methodNamesSet = new HashSet<>();
		this.modStmts = new HashSet<>();
	}
	
	public void addMethod(String metName, List<Integer> stmtList, Set<Integer> branchLineNum)
	{
		// Add method name to the set
		this.methodNamesSet.add(metName);
		
		// Copy all lines for function metName
		for(int i=0;i<stmtList.size();i++)
		{
			if(!this.stmtsList.contains(stmtList.get(i)))
				this.stmtsList.add(stmtList.get(i));
		}
		
		// Copy all branches line numbers
		for (Integer branchIdx : branchLineNum) 
		{
		    this.branchLineNumSet.add(branchIdx);
			// Each will have a True or False execution
			this.branchPathsSet.add(branchIdx+"T");
			this.branchPathsSet.add(branchIdx+"F");
		}
	}
	
	public boolean isBranchLine(int lineNum)
	{
		if(branchLineNumSet.size() > 0)
		{	
			return branchLineNumSet.contains(lineNum);
		}
		else
			return false;
	}
	
	public int nextLineForBranch(int lineNum)
	{
		int value;
		
		int idx = this.stmtsList.indexOf(lineNum);
		
		if(idx > 0 && (idx < this.stmtsList.size() - 1))
		{
			value = this.stmtsList.get(idx+1);
		}
		else
		{
			value = -1;
		}
		return value;
	}
	
	public int getTotalLines()
	{
		return this.stmtsList.size();
	}
	
	public int getTotalMethods()
	{
		return this.methodNamesSet.size();
	}
	
	public int getTotalBranches()
	{
		return this.branchPathsSet.size();
	}
	
	public String getClassName()
	{
		return this.className;
	}
	
	public Set<String> getMethodSet()
	{
		return methodNamesSet;
	}
	
	public List<Integer> getStmtList()
	{
		return stmtsList;
	}
	
	public Set<String> getbranchPathSet()
	{
		return branchPathsSet;
	}
	
	public void addModStmt(int modLine)
	{
		this.modStmts.add(modLine);
	}
	
	public Set<Integer> getModStmts()
	{
		return modStmts;
	}
	
	public boolean containsModStmt(int modLine)
	{
		return modStmts.contains(modLine);
	}
	
}
