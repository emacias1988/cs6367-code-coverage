package com.emac.parser;


import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.Hashtable;
import java.util.List;
import java.util.Set;

public final class CoverageParser
{
	static boolean logDataFlag = false;
	
	static String jUnitTestClass;
	
	// Store all the classes static information including line number, method and branch lines (KEY=>Class name)
	static Hashtable<String,ClassData> classesStaticInfoHash;
	// Store all the classes coverage (KEY=>Class name)
	static Hashtable<String,CoverageCalc> totalClassCoverageHash;
	// Store all the class coverage for single test
	static Hashtable<String,CoverageCalc> singleTestClassCoverageHash;
	// List with all the test coverage 
	static List<TestData> testList;

	// Current Coverage Calc
	static CoverageCalc curCoverageCalc;
	// Current class data
	static ClassData curClassData;
	// Current JUnit test
	static TestData curTest;
	
	// Previous line number
	static int prevLineNumber;
	
	public final static boolean isLogDataFlag()
	{
		return logDataFlag;
	}
	
	
	public final static void testSuiteStart()
	{
		// Clear all existing objects
		
		// Create ClassesHash
		classesStaticInfoHash = new <String,ClassData>Hashtable();
		
		// Create the hashtable with the coverage for all classes
		totalClassCoverageHash = new <String,CoverageCalc>Hashtable();
		
		testList = new <TestData>ArrayList();
		
		// Stop logging data
		logDataFlag = false;
		
		// Initialize prev Line
		prevLineNumber = 0;
	}
	
	public final static void testSuiteFinish()
	{
		// Parse and print all the data
		printTotalCoverage();
		
		// Stop logging data
		logDataFlag = false;
	}
	
	public final static void printTotalCoverage()
	{
		int totalMethod=0, totalStmt=0, totalBranches=0;
		int methodCovered=0, stmtCovered=0, branchCovered=0;
		int totalMethodTemp=0, totalStmtTemp=0, totalBranchesTemp=0;
		int methodCoveredTemp=0, stmtCoveredTemp=0, branchCoveredTemp=0;
		
		Set<String> keys = totalClassCoverageHash.keySet();
        for(String key: keys){
        	totalMethodTemp = totalClassCoverageHash.get(key).getTotalMethod();
        	totalStmtTemp = totalClassCoverageHash.get(key).getTotalStmt();
        	totalBranchesTemp = totalClassCoverageHash.get(key).getTotalBranch();
        	methodCoveredTemp = totalClassCoverageHash.get(key).getMethodCovered();
        	stmtCoveredTemp = totalClassCoverageHash.get(key).getStmtCovered().size();
        	branchCoveredTemp = totalClassCoverageHash.get(key).getBranchCovered();
        	
        	System.out.println("|---------------------- Class Analysis -----------------------|");
            System.out.println("Coverage Analysis for "+key+":");
            
            System.out.println("Method Cov: "+methodCoveredTemp+"/"+totalMethodTemp+" Stmt Cov: "+stmtCoveredTemp+"/"+totalStmtTemp+" Branch Cov: "+branchCoveredTemp+"/"+totalBranchesTemp);
            System.out.println("|-------------------------------------------------------------|");
            
        	totalMethod+= totalMethodTemp;
        	totalStmt+= totalStmtTemp;
        	totalBranches+= totalBranchesTemp;
        	methodCovered+= methodCoveredTemp;
        	stmtCovered+= stmtCoveredTemp;
        	branchCovered+= branchCoveredTemp;
        }

		// Print Test that need to be retested
		if(System.getProperty("cfgClassNames") != null)
		{
			System.out.println("\n|-------------------------------------------------------------|");
			System.out.println("<RTS> Regression Test Selection");
			System.out.println("<RTS> The following test cases were impacted:");
			for(int i=0;i<testList.size();i++)
			{
//				System.out.println("\n|++++++++++++++++++++++ Test Analysis ++++++++++++++++++++++| ");
//				System.out.println("Test Name:"+testList.get(i).getTestname());
				Hashtable<String,CoverageCalc> temp = testList.get(i).getTestCodeCoverage();
				
				Set<String> keyTemp = temp.keySet();
				for(String key: keyTemp){
//		        	System.out.println("|---------------------- Class Analysis ----------------------|");
//		            System.out.println("Coverage Analysis for "+key+":");
					// Check if any class for the test had a modified class
		            if(temp.get(key).isRTSFlag())
		            {
		            	 System.out.println("<RTS> Test "+testList.get(i).getTestname());
		            	 break;
		            }
				}
			}
			System.out.println("|-------------------------------------------------------------|\n");
		}
        
        System.out.println("|-------------------- Total Code Analysis --------------------|");
        System.out.println("Total Coverage Analysis:");
        System.out.println("Method Cov: "+methodCovered+"/"+totalMethod+" Stmt Cov: "+stmtCovered+"/"+totalStmt+" Branch Cov: "+branchCovered+"/"+totalBranches);
        System.out.println("|-------------------------------------------------------------|");
	}
	
	public final static void testCaseStart(String testName)
	{
		// Start logging data
		logDataFlag = true;
		
		// Create a new test case
		curTest = new TestData(testName);
		
		// Add current test to List
		testList.add(curTest);
		
		// Create the hashtable with the coverage for all classes
		singleTestClassCoverageHash = new <String,CoverageCalc>Hashtable();
	}
	
	public final static void testCaseFinish()
	{
		// Stop logging data
		logDataFlag = false;
		
		// Store the coverage results
		curTest.storeData(singleTestClassCoverageHash);
	}
	
	public final static void logClassInfo(String className, String methodName, List<Integer> stmtList, Set<Integer> branchLines)
	{
		if(logDataFlag == true)
		{
			// Check if class does not exist
			if(classesStaticInfoHash.get(className) == null)
			{
				curClassData = new ClassData(className);
				
				// Add Mod Stmt to the class
				if(System.getProperty("cfgClassNames") != null && System.getProperty("cfgClassNames").contains(className.replace("/", ".")))
				{
					// Check if there are dangerous edges
					String classesProp = System.getProperty("cfgClassNames");
					String[] classPropArray = classesProp.split(",");
					
					// Copy the line numbers to dangerEdge
					for(String classProp : classPropArray)
					{
						if(classProp.contains(className.replace("/", ".")))
						{
							if(classProp.contains("[") && classProp.contains("]") && classProp.length() > 2)
							{
								classProp = classProp.substring(classProp.indexOf("[")+1, classProp.length()-1);
								String[] dangEdgesString = classProp.split("/");
								// Copy Edges 
								for(int i=0; i<dangEdgesString.length; i++)
								{
									curClassData.addModStmt(Integer.parseInt(dangEdgesString[i]));
								}
							}
							break;
						}
					}
				}
				
				classesStaticInfoHash.put(className,curClassData);
				curCoverageCalc = new CoverageCalc(className,curClassData);
				totalClassCoverageHash.put(className, curCoverageCalc);
			}
			else
			{
				curClassData = classesStaticInfoHash.get(className);
			}
			
			// Add Method
			curClassData.addMethod(methodName, stmtList, branchLines);
		}
	}

	public final static void logMethod(String className, String s)
	{
		if(logDataFlag == true && classesStaticInfoHash.get(className) != null)
		{
			if(singleTestClassCoverageHash.get(className) == null)
			{
				ClassData tempClassData = classesStaticInfoHash.get(className);
				CoverageCalc tempCovCalc = new CoverageCalc(className,tempClassData);
				singleTestClassCoverageHash.put(className,tempCovCalc);
			}
			
			// Add method covered
			totalClassCoverageHash.get(className).addMethod(s);
			singleTestClassCoverageHash.get(className).addMethod(s);
		}
	}
	
	public final static void logLine(String className, String s)
	{
		if(logDataFlag == true && classesStaticInfoHash.get(className) != null && prevLineNumber != Integer.parseInt(s))
		{
			prevLineNumber = Integer.parseInt(s);
			
			if(singleTestClassCoverageHash.get(className) == null)
			{
				ClassData tempClassData = classesStaticInfoHash.get(className);
				CoverageCalc tempCovCalc = new CoverageCalc(className,tempClassData);
				singleTestClassCoverageHash.put(className,tempCovCalc);
			}
			
			// Add method covered
			totalClassCoverageHash.get(className).addLine(Integer.parseInt(s));
			singleTestClassCoverageHash.get(className).addLine(Integer.parseInt(s));
		}
	}
	
	public final static void logLN(String fileName, String s)
	{
		CoverageParser.createDirectory();
		File logFile = new File("./logs/"+fileName);
        try {
            FileWriter fw = new FileWriter(logFile.getAbsoluteFile(),true);
            String date = new Date().toString();
            fw.write(date+" : "+s);
            fw.write(System.lineSeparator());
            fw.close();
        } catch (IOException ex) {
            System.err.println("Error logging:"+s+". Ex:"+ex);
        }	
	}
	
	public final static void createDirectory()
	{
		File logDir = new File("logs/other");
		if (!logDir.exists()) 
		{
		    try{
		        logDir.mkdirs();
		    } catch(SecurityException se){
		        //handle it
		    }
		}
	}
}

