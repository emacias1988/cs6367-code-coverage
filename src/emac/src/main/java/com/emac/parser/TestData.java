package com.emac.parser;

import java.util.Hashtable;
import java.util.List;
import java.util.Set;

public class TestData {

	// Test name
	private String testname;
	
	// Test Coverage
	private Hashtable<String,CoverageCalc> testCodeCoverage;
	
	public TestData(String testName)
	{
		// Test Name
		this.testname = testName;
	}
	
	public void storeData(Hashtable<String,CoverageCalc> testCoverage)
	{
		this.testCodeCoverage = testCoverage;
	}
	
	public String getTestname()
	{
		return testname;
	}
	
	public Hashtable<String,CoverageCalc> getTestCodeCoverage()
	{
		return testCodeCoverage;
	}
}
