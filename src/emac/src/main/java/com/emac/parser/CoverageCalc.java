package com.emac.parser;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class CoverageCalc 
{
	private String className;
	private ClassData classData;
	private int branchLine;
	private int branchNextLine;
	private boolean rtsFlag;
	
	// Method Coverage
	private Set<String> methodCovSet;
	// Stmt Coverage
	private Set<Integer> stmtCovSet;
	// Branch Coverage
	private Set<String> branchCovSet;
	
	public CoverageCalc(String className, ClassData cData)
	{
		this.className = className;
		this.classData = cData;
		
		this.methodCovSet = new HashSet<>();
		this.stmtCovSet = new HashSet<>();
		this.branchCovSet = new HashSet<>();
		
		this.branchNextLine = -1;
		this.branchLine = -1;
		
		this.rtsFlag = false;
	}
	
	public void addMethod(String pathInfo)
	{
		// Add method to covered set
		this.methodCovSet.add(pathInfo);
	}
	
	public void addLine(int lineNumber)
	{
		this.stmtCovSet.add(lineNumber);
		
		if(this.classData.containsModStmt(lineNumber))
		{
			this.rtsFlag = true;
		}
		
		// Check if previous line was a branch statement
		if(this.branchNextLine != -1)
		{
			// Check if the branch was true or false
			if(lineNumber == this.branchNextLine)
			{
				this.branchCovSet.add(this.branchLine+"T");
			}
			else
			{
				this.branchCovSet.add(this.branchLine+"F");
			}
			// Clear line
			this.branchNextLine = -1;
			this.branchLine = -1;
		}
		
		// Check if line number is branch
		if(this.classData.isBranchLine(lineNumber))
		{
			// Store current line
			this.branchLine = lineNumber;
			// Store what the next  value should be
			this.branchNextLine = this.classData.nextLineForBranch(lineNumber);
		}
	}
	
	public int getMethodCovered()
	{
		return this.methodCovSet.size();
	}
	
	public int getTotalMethod()
	{
		return this.classData.getTotalMethods();
	}
	
	public Set<Integer> getStmtCovered()
	{
		return this.stmtCovSet;
	}
	
	public int getTotalStmt()
	{
		return this.classData.getTotalLines();
	}
	
	public int getBranchCovered()
	{
		return this.branchCovSet.size();
	}
	
	public int getTotalBranch()
	{
		return this.classData.getTotalBranches();
	}

	public String getClassAnalysis()
	{
		return ("Methods: \n "+classData.getMethodSet().toString()+"\nStmts: \n"+classData.getStmtList().toString()+"\nBranch: \n"+classData.getbranchPathSet().toString());
	}
	
	public boolean isRTSFlag()
	{
		return this.rtsFlag;
	}
}
