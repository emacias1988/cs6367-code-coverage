Welcome to the EMAC
=========================

   EMAC covers a wide range of functionality grouped in the
   following packages:

 * _agent_ - A java agent class manipulating the java bytecode to track the
     static and dynamic code coverage metrics.

 * _junit.listener_ - Tracks the start / end of the JUnit Test cases.

 * _parser_ - A Dynamic Regression Test Selection implementation tracking
     dynamic code coverage metric, generates Control Flow Graphs, and
     reports the impacted tests cases for known modified line in existing
     java classes.

Requirements:
--------------
  EMAC requires ASM 5.0.3 or later;
  Maven 3.3.9, JUnit 4.12, GraphViz 2.38 and Java JDK 1.8 is required to build.

Getting Started
---------------
 1. Clone ( or fork ) the repo from https://bitbucket.org/emacias1988/cs6367-code-coverage
 2. Launch the Java application (including Maven POM.xml and JUnit test cases)
 3. Make the following modifications to the applications's POM.xml:
    a.Add a dependency under the dependencies section:

      <dependency>
        <groupId>com.coverage.emac</groupId>
        <artifactId>emac</artifactId>
        <version>0.1-SNAPSHOT</version>
      </dependency>

    b.Add a plugin under the <build>/<plugins> section. The $(emac.path) is the
      location where the repo in step 1 is downloaded to. The $(groupId) should
      be the name of the common package name prefix for the classes to test. The
      $(class.name) under the cfgClassNames section is used to generate the CFG
      graphs. Enter the name of the classes to generate the CFG graphs. To
      generate the test impacted by known modified lines add the line numbers
      following the class name for the cfgClassName separated by '/'.

      For example to generate the CFG for classes BinarySearch (lines 4, and 5)
      and QuickSort (line 8). The cfgClassNames value would be
      com.pack.name.BinarySearch[4/5],com.pack.name.QuickSort[8].

      The plugin structure is shown below:
      <plugin>
        <groupId>org.apache.maven.plugins</groupId>
        <artifactId>maven-surefire-plugin</artifactId>
        <version>2.16</version>
        <configuration>
          <argLine>-javaagent:${emac.path}/src/emac/target/emac-0.1-SNAPSHOT.jar -Xss400m</argLine>
          <systemProperties>
            <property>
              <name>testProjectPackageName</name>
              <value>${groupId} </value>
            </property>
            <property>
              <name> cfgClassNames </name>
              <value> ${class.name}[lines #] </value>
            </property>
          </systemProperties>
          <properties>
            <property>
              <name>listener</name>
              <value>com.emac.junit.listener.JUnitSniffer</value>
            </property>
          </properties>
        </configuration>
      </plugin>

 4. Run 'mvn test' command and see the EMAC output as shown in the example below
 5. See the $(emac.path)/src/other project as an example. When running the mvn test
    command the console will display:

<CFG> Generated CFG BinarySearch.<init>()V.dot Path: /target/CFG/com/emac/other/BinarySearch
<CFG> Generated CFG BinarySearch.contains([II)Z.dot Path: /target/CFG/com/emac/other/BinarySearch
Tests run: 5, Failures: 0, Errors: 0, Skipped: 0, Time elapsed: 0.073 sec - in other.BinarySearchTest
|---------------------- Class Analysis -----------------------|
Coverage Analysis for com/emac/other/BinarySearch:
Method Cov: 2/2 Stmt Cov: 14/14 Branch Cov: 8/8
|-------------------------------------------------------------|

|-------------------------------------------------------------|
<RTS> Regression Test Selection
<RTS> The following test cases were impacted:
|-------------------------------------------------------------|

|-------------------- Total Code Analysis --------------------|
Total Coverage Analysis:
Method Cov: 2/2 Stmt Cov: 14/14 Branch Cov: 8/8
|-------------------------------------------------------------|

Results :

Tests run: 5, Failures: 0, Errors: 0, Skipped: 0

[INFO] ------------------------------------------------------------------------
[INFO] BUILD SUCCESS
[INFO] ------------------------------------------------------------------------
[INFO] Total time: 2.449 s
[INFO] Finished at: 2016-12-09T21:56:38-06:00
[INFO] Final Memory: 23M/312M
[INFO] ------------------------------------------------------------------------
