
package other;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import com.emac.other.BinarySearch;

public class BinarySearchTest {
	
    @BeforeClass
    public static void setUpBeforeClass() throws Exception {

    }

    @AfterClass
    public static void tearDownAfterClass() throws Exception {
    }

    @Before
    public void setUp() throws Exception {
    }

    @After
    public void tearDown() throws Exception {
    }

    @Test
    public void testBinomial1()
    {
    	BinarySearch bin = new BinarySearch();
    	int a[] = {1,2,3};

    	assertEquals( true, bin.contains(a, 2));
    }
    
    @Test
    public void testBinomial2()
    {
    	BinarySearch bin = new BinarySearch();
    	int a[] = {1,2,3};

    	assertEquals( true, bin.contains(a, 1));
    }
    
    @Test
    public void testBinomial3()
    {
    	BinarySearch bin = new BinarySearch();
    	int a[] = {1,2,3};

    	assertEquals( true, bin.contains(a, 3));
    }
    
    @Test
    public void testBinomial4()
    {
    	BinarySearch bin = new BinarySearch();
    	int a[] = {1,2,3};

    	assertEquals( false, bin.contains(a, 4));
    }
    
    @Test
    public void testBinomial5()
    {
    	BinarySearch bin = new BinarySearch();
    	int a[] = {};

    	assertEquals( false, bin.contains(a, 4));
    }

}
